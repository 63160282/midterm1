/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shopeecard;

/**
 *
 * @author User
 */
public class TastCard {
    public static void main(String[] args) {
        Card card = new Card("Jirarat","Phoophamorn");
        System.out.println(card.toString());
        
        ClassicCard classic = new ClassicCard();
        System.out.println("ClassicCard");
        System.out.println("You get a discount: "+classic.discount((float) 2000.0)+" Baht");
        System.out.println("You get a coins: +"+classic.discount((int) 1000));
        card.Line();
        SilverCard silver = new SilverCard();
        System.out.println("SilverCard");
        System.out.println("You get a discount: "+silver.discount((float) 1000.0)+" Baht");
        System.out.println("You get a discount: "+silver.discount((float) 500.0)+" Baht");
        System.out.println("You get a coins: +"+silver.discount(1000));
        card.Line();
        GoldCard gold = new GoldCard();
        System.out.println("GoldCard");
        System.out.println("You get a discount: "+gold.discount((float) 1000.0)+" Baht");
        System.out.println("You get a discount: "+gold.discount((float) 500.0)+" Baht");
        System.out.println("You get a coins: +"+gold.discount((int)2000));
        card.Line();
        PlatinumCard platinum = new PlatinumCard();
        System.out.println("PlatinumCard");
        System.out.println("You get a discount: "+platinum.discount((float) 2500.0)+" Baht");
        System.out.println("You get a discount: "+platinum.discount((float) 500.0)+" Baht");
        System.out.println("You get a coins: +"+platinum.discount((int)2000));
    }
    
}
