/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shopeecard;

/**
 *
 * @author User
 */
public class Card {

    private String FirstName;
    private String LastName;

    public Card(String FirstName, String LastName) {
        this.FirstName = FirstName;
        this.LastName = LastName;
    }

    public Card() {
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    @Override
    public String toString() {
        return String.format("My card name: " + FirstName + " " + LastName);
    }

    public float discount(float amount) {
        if (amount >= 1000) {
            return (int) (amount * 0.05);
        } else {
            return 0;
        }
    }

    public float discount(int amount) {
        if (amount >= 1000) {
            return (float) (amount/100);
        } else {
            return 0;
        }
    }

    public void Line() {
        System.out.println("____________________________");
    }

}
