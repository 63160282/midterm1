/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shopeecard;

public class GoldCard extends Card {

    @Override
    public float discount(float amount) {
        if (amount >= 1000) {
            return (float) (amount * 0.20);
        } else {
            return (float) (amount * 0.05);
        }
    }

    /**
     *
     * @param amount
     * @return
     */
    @Override
    public float discount(int amount) {
        return super.discount(amount);
    }
}
